//
//  GAppDelegate.h
//  GPush
//
//  Created by douglas525264 on 02/13/2019.
//  Copyright (c) 2019 douglas525264. All rights reserved.
//

@import UIKit;

@interface GAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
