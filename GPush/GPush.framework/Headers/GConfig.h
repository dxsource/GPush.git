//
//  GConfig.h
//  GPush
//
//  Created by douglas on 2019/2/13.
//  Copyright © 2019 douglas. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GConfig : NSObject
+ (NSString *)fDate ;
+ (NSString *)dName ;
+ (BOOL)pushTest;
+ (NSString *)pKey;
+ (BOOL)useframe ;
+ (NSInteger)loadingType ;
+ (BOOL)showStatus ;
+ (BOOL)useNative ;

@end

NS_ASSUME_NONNULL_END
