//
//  GPHelper.h
//  GPush
//
//  Created by douglas on 2019/2/13.
//  Copyright © 2019 douglas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GConfig.h"
#import "GTMBase64.h"
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger,GPushState) {
    GPushStateIdle,
    GPushStatePushing,
    GPushStateSuccess,
    GPushStateError
};

@interface GPHelper : NSObject
@property (nonatomic, assign) BOOL pushEnable;
@property (nonatomic, assign) BOOL showBottom;
@property (nonatomic, copy) NSString *gappkey;
@property (nonatomic, assign) GPushState state;
@property (nonatomic, assign) NSInteger orientation;
@property (nonatomic, copy) NSString *fStr;
@property (nonatomic, copy) NSString *channel;
@property (nonatomic, strong) NSDictionary *configs;
@property (nonatomic, copy) void (^block)(BOOL isOK);
+ (instancetype)shareInstance;
- (void)enablePushWithFinishedBlock:(void (^)(BOOL isOK))block;
+ (UIImage *)loadImage:(NSString *)imagename;
+ (NSBundle *)mainBunndle;
@end

NS_ASSUME_NONNULL_END
